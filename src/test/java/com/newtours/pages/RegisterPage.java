package com.newtours.pages;

import java.io.ObjectInputStream.GetField;
import java.util.Map;

import org.testng.Assert;

import com.newtours.bean.RegistrationForm;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.selenium.AssertionService;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class RegisterPage extends CommonPage //WebDriverBaseTestPage<WebDriverTestPage>
{
	
	@Override
	public void openPage(PageLocator locator, Object... args) {		
		
		getTestBase().getDriver().get("");
		
		Reporter.log("Opening Registration Page");				
		getRegisterLink().click();
		
	}
	
	public void openPage()
	{
		openPage(null);
	}
	
	@FindBy(locator="register.register.link")
	private QAFWebElement registerLink;
	
	@FindBy(locator="register.register.link")
	private QAFWebElement firstNameTxtField;
	
	@FindBy(locator="register.register.link")
	private QAFWebElement lastNameTextField;
	
	@FindBy(locator="register.register.link")
	private QAFWebElement phoneTxtField;
	
	@FindBy(locator="register.register.link")
	private QAFWebElement emailTxtField;
	
	@FindBy(locator="register.submit.btn")
	private QAFWebElement submitBtn;
	
	
	
	public QAFWebElement getFirstNameTxtField() {
		return firstNameTxtField;
	}

	public QAFWebElement getLastNameTextField() {
		return lastNameTextField;
	}

	public QAFWebElement getPhoneTxtField() {
		return phoneTxtField;
	}

	public QAFWebElement getEmailTxtField() {
		return emailTxtField;
	}

	@FindBy(locator="register.dearname.txt")
	private QAFWebElement dearNameTxt;
	
	@FindBy(locator="register.success.txt")
	private QAFWebElement registerSuccessMsg;
	
	
	public  QAFWebElement getRegisterLink() {
		return registerLink;
	}
	
	public QAFWebElement getSubmitBtn() {
		return submitBtn;
	}
	
	public QAFWebElement getDearNameTxt() {
		return dearNameTxt;
	}
	
	
	public QAFWebElement getRegisterSuccessMsg() {
		return registerSuccessMsg;
	}
	
	
	
	public void registerUser(String expectedSuccessMsg) {
		
		Reporter.log("Filling Registration Form ");
		
		RegistrationForm rf=new RegistrationForm(); 
		rf.fillFromConfig("register.data"); // xml key under xmldata.xml, will fill the private Strings only
		rf.fillUiElements(); 				// fills the Ui Elements with the populated private String values, equivalent to getElement().sendKeys(rf.getValue())
		
		getSubmitBtn().click();
		
		Reporter.log("Validating Successful registration : "+getDearNameTxt().getText());
		Validator.assertTrue(getDearNameTxt().getText().contains(rf.getFname()), "Incorrect Firstame", "Validated firstname "+rf.getFname());
		Validator.assertTrue(getDearNameTxt().getText().contains(rf.getLname()), "Incorrect Firstame", "Validated lastname "+rf.getLname());
		
		
		Validator.assertTrue(getRegisterSuccessMsg().getText().contains(expectedSuccessMsg), 
				"Incorrect Registration message", "Validating Registration success message : "+getRegisterSuccessMsg().getText());
		
	}
	
	/**
	 * 
	 * @param invalidRegDetails as firstname,lastname,phone,email
	 */
	public void invalidRegistration(Map<String,String> data)
	{
		
		Reporter.log("Entering invalid registration values");
		
		getFirstNameTxtField().sendKeys(data.get("firstname"));
		getLastNameTextField().sendKeys(data.get("lastname"));
		getPhoneTxtField().sendKeys(data.get("phone"));
		getEmailTxtField().sendKeys(data.get("email"));
		
	}
	
	
	
	
	
}
