package com.newtours.pages;

import org.testng.Assert;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.selenium.AssertionService;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class ContactPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {		
	}
	
	@FindBy(locator="contact.const.txt")
	private QAFWebElement underConstructionTxt;
	
	public  QAFWebElement getUnderConstructionTxt() {
		return underConstructionTxt;
	}
	
	
	
}
