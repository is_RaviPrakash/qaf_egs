package com.newtours.pages;

import org.openqa.selenium.WebElement;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.selenium.AssertionService;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage>{

	@Override
	public void openPage(PageLocator locator, Object... args) {
		driver.get("");
	}
	
	public void openPage() {
		openPage(null);
	}
	

	
	@FindBy(locator="home.username.txt")
	private QAFWebElement usernameTxtField;
	
	@FindBy(locator="home.pwd.txt")
	private QAFWebElement pwdTxtField;
	
	@FindBy(locator="home.signin.btn")
	private QAFWebElement signInBtn;
	
	
	
	@FindBy(locator="id=submit_button")
	private QAFWebElement submitButton;
	
	@FindBy(locator="home.featuredDest.img")
	private QAFWebElement featuredDestImg;
	
	@FindBy(locator="home.specialsDesc.lbl")
	private QAFWebElement specialsDesrciption;
	
	@FindBy(locator="home.specialsAmt.lbl")
	private QAFWebElement specialsAmts;
	
	@FindBy(locator="home.tourTip.lbl")
	private QAFWebElement tourTip;
	
	@FindBy(locator="home.findaflight.img")
	private QAFWebElement findAFlight;
	
	@FindBy(locator="home.destination.link")
	private QAFWebElement destination;
	
	@FindBy(locator="home.vacations.link")
	private QAFWebElement vacations;
	
	@FindBy(locator="home.register.link")
	private QAFWebElement register;
	
	@FindBy(locator="home.links.link")
	private QAFWebElement links;
	
	
	
	public void login(String uname,String pwd)
	{
		getUsernameTxtField().sendKeys(uname);
		getPwdTxtField().sendKeys(pwd);
		
		getSignInBtn().click();
		
	}
	
	
	public QAFWebElement getUsernameTxtField() {
		return usernameTxtField;
	}


	public QAFWebElement getPwdTxtField() {
		return pwdTxtField;
	}
	
	public QAFWebElement getSignInBtn() {
		return signInBtn;
	}


	public QAFWebElement getSubmitButton() {
		return submitButton;
	}


	public QAFWebElement getSpecialsDesrciption() {
		return specialsDesrciption;
	}


	public QAFWebElement getSpecialsAmts() {
		return specialsAmts;
	}


	public QAFWebElement getTourTip() {
		return tourTip;
	}


	public QAFWebElement getFindAFlight() {
		return findAFlight;
	}


	public QAFWebElement getDestination() {
		return destination;
	}


	public QAFWebElement getVacations() {
		return vacations;
	}


	public QAFWebElement getRegister() {
		return register;
	}


	public QAFWebElement getLinks() {
		return links;
	}


	
	
	
	
	public void verifyPageTitle()
	{
		String pageTitle=getTestBase().getDriver().getTitle();
		
		System.out.println("Page Title : "+pageTitle);
		
		featuredDestImg.waitForPresent(15);		
		
		// Validator.
		
		Reporter.log("Report this");
	}


	public QAFWebElement getFeaturedDestImg() {
		return featuredDestImg;
	}

	public void setFeaturedDestImg(QAFWebElement featuredDestImg) {
		this.featuredDestImg = featuredDestImg;
	}


	
	
	
	

}
