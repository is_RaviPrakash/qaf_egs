package com.newtours.pages;

import org.testng.Assert;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.selenium.AssertionService;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class CommonPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	private String pageTitle;
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {		
	}
	
	@FindBy(locator="common.register.link")
	private QAFWebElement registerLink;
	
	@FindBy(locator="common.signout.btn")
	private QAFWebElement signoutBtn;
	
	@FindBy(locator="common.contact.link")
	private QAFWebElement contactLink;
	
	public QAFWebElement getRegisterLink() {
		return registerLink;
	}

	public void verifyLoginSuccess() {
		
		getTestBase().addAssertionsLog("Verifying Login Success");
		
		pageTitle=getTestBase().getDriver().getTitle();
		
		Validator.assertTrue(pageTitle.contains("Find a Flight"), "Login Failed ", "Logged in Successfully");
		
	}
	
	
	public void verifyLogoutSuccess() {
		
		getTestBase().addAssertionsLog("Verifying Logout Success");
		
		pageTitle=getTestBase().getDriver().getTitle();
		
		Validator.assertTrue(pageTitle.contains("Mercury Tours"), "Logout Failed ", "Logout Successful");
		
	}
	
	public QAFWebElement getSignoutBtn()
	{
		return signoutBtn;
	}
	
	public  void logout()
	{
		Reporter.log("Clicking on Signout button");
		getSignoutBtn().click();
	}
	
	public QAFWebElement getContactLink()
	{
		return contactLink;
	}
	
	public void navigateToContacts() {
		
		Reporter.log("Clicking on Contact Link");
		getContactLink().click();
	}
	
	
}
