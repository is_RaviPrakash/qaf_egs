package com.newtours.bean;

import com.qmetry.qaf.automation.data.BaseFormDataBean;
import com.qmetry.qaf.automation.ui.annotations.UiElement;
import com.qmetry.qaf.automation.ui.annotations.UiElement.Type;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;
import com.qmetry.qaf.automation.util.Randomizer;

public class RegistrationForm extends BaseFormDataBean {
	
	@UiElement(fieldLoc="register.fname.field")
	private String fname;	
	
	@UiElement(fieldLoc="register.lname.field")
	private String lname;	
	
	@UiElement(fieldLoc="register.phone.field")
	private String phone;
	
	@UiElement(fieldLoc="register.email.field")
	private String email;
	
	@UiElement(fieldLoc="register.addressline1.field") // viewType=Type.optionbox for radio button 
	private String addressLine1;
	
	@UiElement(fieldLoc="register.addressline2.field")
	private String addressLine2;
	
	@UiElement(fieldLoc="register.city.field")
	private String city;
	
	@UiElement(fieldLoc="register.state.field")
	private String state;
	
	@UiElement(fieldLoc="register.postal.field")
	private String postal;
	
	@UiElement(fieldLoc="register.country.select",fieldType=Type.selectbox)
	private String country;	
	
	@UiElement(fieldLoc="register.uname.field")
	private String uname;
	
	@UiElement(fieldLoc="register.pwd.field")
	private String pwd;
	
	@UiElement(fieldLoc="register.confirmpwd.field")
	private String confpwd;
		
	
	public String getFname() {
		return fname;
		
	}
	
	public String getLname() {
		return lname;
	}
	
	
	
}
