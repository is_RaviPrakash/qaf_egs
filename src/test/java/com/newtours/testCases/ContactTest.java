package com.newtours.testCases;

import static com.qmetry.qaf.automation.step.CommonStep.get;
import org.testng.annotations.Test;
import com.newtours.pages.CommonPage;
import com.newtours.pages.ContactPage;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

public class ContactTest extends WebDriverTestCase {

	@Test(description="On Clicking 'CONTACT' link on the homepage, verify 'Under Construction' message is displayed ")
	public void testContact() {
		
		String expectedContactMsg=getProps().getString("contact.msg"),actualMsg;
					
		get("");
		
		CommonPage cp=new CommonPage();
		cp.navigateToContacts();
		
		ContactPage ctp=new ContactPage();
		actualMsg=ctp.getUnderConstructionTxt().getText();
		
		getTestBase().addTestStepLog("Validating Contact Page message : "+actualMsg);
		
		assertTrue(actualMsg.contains(expectedContactMsg), "Invalid message", "Contacts page message found");
			
	}
}
