package com.newtours.testCases;

import java.util.Map;
import org.testng.annotations.Test;
import com.newtours.pages.RegisterPage;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

public class InvalidRegisterTest extends WebDriverTestCase {
	
	@QAFDataProvider(dataFile="resources/testdata/testdata.xls",hasHeaderRow=true)
	@Test(description="Verify user registration by filling invalid data")
	public void testInvalidRegister(Map<String,String> data) {
		
		String	expectedSuccessMsg=data.get("expectedSuccessMsg"),desc=data.get("desc");	
		
		RegisterPage rp=new RegisterPage();
		rp.openPage();
		
		getTestBase().addTestStepLog("Entering data for "+ desc);
				
		rp.invalidRegistration(data);
		
		assertTrue(rp.getRegisterSuccessMsg().getText().contains(expectedSuccessMsg), "Not registered", "User Registered");
		
		
			
	}
}
