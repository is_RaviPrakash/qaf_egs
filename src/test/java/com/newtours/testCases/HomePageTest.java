package com.newtours.testCases;

import static com.qmetry.qaf.automation.step.CommonStep.get;
import static com.qmetry.qaf.automation.step.CommonStep.verifyLinkWithPartialTextPresent;
import static com.qmetry.qaf.example.steps.StepsLibrary.searchFor;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.newtours.pages.HomePage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class HomePageTest extends WebDriverTestCase {

	/**
	 * Check different components and links in the HomePage, i.e 
	 * Featured Destination, Special, Tour tips, Find a flight, Destination, Vacations, Register, Links
	 * 
	 */
	@Test(description="Check different components and links in the HomePage")
	public void testHomeLinks() {
		
		WebDriverTestBase testBase=getTestBase();	
		System.out.println(" ========== Beginning TestCase =========="); 
		
		testBase.addTestStepLog("Begin TestCase");  
		
		 // QAFExtendedWebDriver driver=getDriver();		
		get("http://newtours.demoaut.com"); // url from application.properties
		
		HomePage hp=new HomePage();
		
		assertTrue(hp.getFeaturedDestImg().isDisplayed(), "Feautured Destination not found ", "Featured Destination Image found");
		assertTrue(hp.getSpecialsDesrciption().isDisplayed(), "Specials Description not found ", "Specials Description Image found");
		assertTrue(hp.getSpecialsAmts().isDisplayed(), "Specials Amount not found ", "Specials Amount found");
		assertTrue(hp.getTourTip().isDisplayed(), "Tour Tip not found ", "Tour Tip found");
		assertTrue(hp.getFindAFlight().isDisplayed(), "Find a Flight section not found ", "Find a Flight section found");
		assertTrue(hp.getDestination().isDisplayed(), "Destinations link not found ", "Destinations link found");
		assertTrue(hp.getVacations().isDisplayed(), "Vacation link not found ", "Vacations link found");
		assertTrue(hp.getRegister().isDisplayed(), "Register link not found ", "Register link found");
		assertTrue(hp.getLinks().isDisplayed(), "Additional Links not found ", "Additional Links found");
		
		// Validator
		
		// Reporter.log("");
		
		// ========================//
		
		if(true) return;
		get("/");
		searchFor("qaf github infostretch");
		verifyLinkWithPartialTextPresent("qaf");
		
	}
}
