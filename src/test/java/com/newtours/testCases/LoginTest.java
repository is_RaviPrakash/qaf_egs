package com.newtours.testCases;

import org.testng.annotations.Test;
import com.newtours.pages.CommonPage;
import com.newtours.pages.FindAFlightPage;
import com.newtours.pages.HomePage;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

public class LoginTest extends WebDriverTestCase {

	@Test(description="Verify successful Login and Logout ")
	public void testLogin() {
		
		String userName=getProps().getString("login.username"),
					pwd=getProps().getString("login.pwd");
	
		getTestBase().addTestStepLog(" Logging in as Username : "+userName+" pwd : "+pwd);
		
		HomePage hp=new HomePage();
		hp.openPage();
		hp.login(userName, pwd);
		
		FindAFlightPage fafp=new FindAFlightPage();
		fafp.verifyLoginSuccess();
		
		fafp.logout();
		fafp.verifyLogoutSuccess();
		
	}
}
