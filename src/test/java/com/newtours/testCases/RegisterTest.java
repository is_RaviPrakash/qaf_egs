package com.newtours.testCases;

import org.testng.annotations.Test;
import com.newtours.pages.RegisterPage;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.PropertyUtil;

public class RegisterTest extends WebDriverTestCase {

	@Test(description="Verify user registration by filling all valid data")
	public void testLogin() {
		
		String expectedSuccessMsg=getProps().getString("register.successMsg");
		
		RegisterPage rp=new RegisterPage();
		rp.openPage();
		rp.registerUser(expectedSuccessMsg);
			
	}
}
