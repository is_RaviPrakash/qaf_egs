package com.newtours.testCases;

import org.testng.annotations.Test;
import static com.qmetry.qaf.automation.step.CommonStep.get;

import java.util.Map;

import com.newtours.pages.CommonPage;
import com.newtours.pages.HomePage;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class InvalidLoginTest extends WebDriverTestCase {
	
	/**
	 * EGS 370
	 */
	
	@QAFDataProvider(key="login.data")  // using xml key instead of dataFile
	@Test(description="User should not be able to Login with Invalid Username or Invalid Password")
	public void testInvalidLogin(Map<String,String> data)
	{
		String uname=data.get("user_name"),pwd=data.get("password");
		
		HomePage hp=new HomePage();	
		hp.openPage();
				
		getTestBase().addTestStepLog("Validating for "+data.get("desc"));
		hp.login(uname, pwd);
		assertTrue(hp.getUsernameTxtField().isDisplayed(), "Error in Page", "Expected Login Page is displayed");
		
	}
	
	
	
	
}
